Stupid Downloader
=================

[![build status][ci-badge]][ci]

[![gem version][gem-badge]][gem]

A stupid simple library for downloading files using Net::HTTP.

Will attempt to track etag & send If-None-Match header.

## Development

### Prerequisites

- ruby    # rvm install ruby 2.3.0
- bundler # gem install bundler

```
$ git clone git@gitlab.devfu.com:devfu/stupid_downloader.git
$ cd stupid_downloader
$ bundle
$ bundle exec rake spec
```
<!-- links -->
[ci]: https://gitlab.devfu.com/devfu/stupid_downloader/builds "build history"
[gem]: https://rubygems.org/gems/stupid_downloader

<!-- images -->
[ci-badge]: https://gitlab.devfu.com/devfu/stupid_downloader/badges/master/build.svg
[gem-badge]: https://badge.fury.io/rb/stupid_downloader.svg
