require 'fileutils'
require 'net/http'

module StupidDownloader
  class HTTPDownloader

    attr_accessor :file_name
    attr_reader   :directory, :url

    def initialize **options
      options.each { |key, value| send "#{ key }=", value }

      self.directory ||= File.join %w[ private downloads ]
    end

    def directory= value
      @directory = value
      FileUtils.mkdir_p directory
      value
    end

    def download!
      fail 'file_name is required' unless file_name

      File.open file_path, 'w+' do |file|
        Net::HTTP.start(uri.hostname, uri.port) { |http| download http, to: file }
      end
    end

    def etag
      @etag ||= File.exists?(etag_path) ? open(etag_path).read : nil
    end

    def uri
      @uri ||= URI.parse url
    end

    def url= value
      @url = value
      @file_name ||= File.basename value rescue nil
      value
    end

  private

    def download http, **options
      file = options.fetch :to

      http.request request do |response|
        if response.code == '200'
          record_etag response['etag']
          response.read_body { |chunk| file.write chunk }
        else
          fail response.inspect unless response.code == '304'
        end

        return response
      end
    end

    def etag_path
      @etag_path ||= Pathname.new File.join directory, "#{ file_name }.etag"
    end

    def file_path
      @file_path ||= Pathname.new File.join directory, file_name
    end

    def record_etag etag
      File.open(etag_path, 'w+') { |f| f.write etag }
    end

    def request force = false
      @request ||= Net::HTTP::Get.new uri
      @request['If-None-Match'] = etag if etag
      @request
    end

  end
end
