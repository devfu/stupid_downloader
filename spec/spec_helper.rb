Bundler.require :default, :test

require File.expand_path '../../lib/stupid_downloader', __FILE__

RSpec.configure do |config|
  Kernel.srand config.seed

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
    expectations.syntax = :should
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
    mocks.syntax = :should
  end

  config.filter_run :focus
  config.filter_run_excluding ci: false if ENV['CI'] == 'true'

  config.example_status_persistence_file_path = 'spec/data/persistence.txt'
  config.order                                = :random
  config.run_all_when_everything_filtered     = true
end

VCR.configure do |config|
  config.hook_into :webmock
  config.configure_rspec_metadata!

  config.cassette_library_dir     = 'spec/data/cassettes'
  config.default_cassette_options = {
    record: :new_episodes
  }
end
