require 'spec_helper'
require 'securerandom'

module StupidDownloader
  describe HTTPDownloader do

    describe 'errors' do

      it 'must have a file name' do
        ->{ subject.download! }.should raise_exception RuntimeError
      end

      it 'must have a uri' do
        downloader = HTTPDownloader.new file_name: 'foo'
        ->{ downloader.download! }.should raise_exception URI::InvalidURIError
      end
    end

    describe '#directory' do

      it 'defaults to private/downloads' do
        downloader = described_class.new
        downloader.directory.should =='private/downloads'
      end

      it 'can be overridden' do
        downloader = described_class.new directory: 'tmp'
        downloader.directory.should == 'tmp'
      end

    end

    describe '#download!' do

      subject(:downloader) { described_class.new directory: 'tmp', url: 'http://www.dir.ca.gov/dwc/pharmfeesched/pharmfeesched.zip' }

      context 'the file does not exist', vcr: { allow_playback_repeats: true, cassette_name: 'download' } do

        let(:etag) { downloader.send :etag_path }
        let(:file) { downloader.send :file_path }

        before do
          [etag, file].each { |f| f.delete if f.exist? }

          @response = downloader.download! # capture the response body
        end

        it 'downloads the file' do
          file.should exist
        end

        it 'downloads the expected amount of data' do
          file.size.should == @response.content_length
        end

        it 'writes etag to a file also' do
          open(etag).read.should == @response['etag']
        end

      end

      context 'the file does exist', vcr: { allow_playback_repeats: false, cassette_name: 'download' } do

        before do
          # ensure the file exists
          downloader.download!
        end

        context 'file has not been modified since' do

          let(:response) { downloader.download! }

          it 'returns 304 not modified' do
            response.code.should == '304'
          end

        end

      end

    end

    describe '#etag' do

      context 'etag_path does not exist' do

        before do
          subject.stub(:etag_path).and_return "tmp/#{ SecureRandom.hex }"
        end

        its(:etag) { should be_nil }

      end

      context 'etag_path exists' do

        let(:path) { downloader.send :etag_path }

        subject(:downloader) { HTTPDownloader.new directory: 'tmp' }

        before do
          # write some 💩to the etag file
          File.open(path, 'w+') { |f| f.write '"💩💩💩"' }
        end

        its(:etag) { should == '"💩💩💩"' }

      end

    end

  end
end
