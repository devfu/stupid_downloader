# -*- encoding: utf-8 -*-
$:.push File.expand_path('../lib', __FILE__)
require 'stupid_downloader/version'

Gem::Specification.new do |s|
  s.name        = 'stupid_downloader'
  s.version     = StupidDownloader::VERSION
  s.authors     = ['BM5k']
  s.email       = ['me@bm5k.com']
  s.homepage    = 'https://gitlab.devfu.com/devfu/stupid_downloader'
  s.summary     = 'A stupid simple library for downloading files using Net::HTTP.'
  s.description = "Download a file over HTTP & keep track of it's etag"
  s.licenses    = ['MIT']

  s.files         = `git ls-files`.split "\n"
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split "\n"
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename f }
  s.require_paths = ['lib']

  s.add_development_dependency 'rake'
end
